set -e
set -u
source ./scripts/setup_env.sh
echo "Pushing branch ${CI_COMMIT_REF_NAME} to app ${HEROKU_APP}"
git remote add heroku https://heroku:$HEROKU_API_KEY@git.heroku.com/${HEROKU_APP}.git
heroku stack:set container
git push -f heroku HEAD:master