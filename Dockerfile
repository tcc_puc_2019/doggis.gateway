#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY src /usr/app/src
COPY pom.xml /usr/app

ENV EUREKA_HOST ${EUREKA_HOST}
ENV REGISTER_HOST ${REGISTER_HOST}
ENV PLAYLIST_HOST ${PLAYLIST_HOST}
ENV MONITORING_HOST ${MONITORING_HOST}
ENV REPORT_HOST ${REPORT_HOST}

RUN mvn clean package -DskipTests -Dspring.profiles.active=heroku
CMD java -Djava.security.egd=file:/dev/./urandom -Xmx500m -Dspring.profiles.active=heroku -jar /usr/app/target/doggis-gateway-1.0-SNAPSHOT.jar